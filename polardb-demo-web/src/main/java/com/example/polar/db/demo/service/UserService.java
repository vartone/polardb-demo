package com.example.polar.db.demo.service;

import com.example.polar.db.demo.entity.User;

import java.util.List;

/**
 * @author lihuazeng
 */
public interface UserService {
    User queryById(Integer id);

    User insert(User user);

    User queryByIdFromSlave(Integer id);

    List<User> queryByIdsFromSlave(String id);
}
