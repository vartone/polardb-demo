package com.example.polar.db.demo.controller;

import com.example.polar.db.demo.entity.User;
import com.example.polar.db.demo.service.UserService;
import com.example.polar.db.demo.service.impl.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * @author lihuazeng
 */
@RestController
@RequestMapping("users")
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private RedisService<String> cachingService;

    @GetMapping("{id}")
    public User get(@PathVariable Integer id){
        return userService.queryById(id);
    }

    @PostMapping
    public User add(@RequestBody User user){
        return userService.insert(user);
    }

    @GetMapping("caching/{key}")
    public Map<String, String> testProxyKey(@PathVariable String key){
        HashMap<String, String> data = new HashMap<>();
        data.put(key, cachingService.getCacheObject(key));
        return data;
    }

    @GetMapping("caching/test")
    public Object[] testCaching(){
        Object[] arr = new Object[8];
        for (int i = 0; i < 8; i++){
            HashMap<String, String> data = new HashMap<>();
            String key = UUID.randomUUID().toString();
            String value = "2";
            data.put(key, value);
            cachingService.setCacheObject(key, value);
            arr[i] = data;
        }
        return arr;
    }
}
