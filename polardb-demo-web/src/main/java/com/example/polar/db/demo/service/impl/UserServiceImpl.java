package com.example.polar.db.demo.service.impl;

import com.example.polar.db.demo.entity.User;
import com.example.polar.db.demo.repository.UserMapper;
import com.example.polar.db.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author lihuazeng
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired(required = false)
    private UserMapper userMapper;

    @Override
    public User queryById(Integer id) {
        return userMapper.selectById(id);
    }

    @Override
    public User insert(User user) {
        userMapper.insert(user);
        return user;
    }

    @Override
    public User queryByIdFromSlave(Integer id) {
        return userMapper.queryByIdFromSlave(id);
    }

    @Override
    public List<User> queryByIdsFromSlave(String id) {
        return userMapper.queryByIdsFromSlave(id);
    }
}
