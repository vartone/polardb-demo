package com.example.polar.db.demo.service;

import com.example.polar.db.demo.entity.Article;
import com.example.polar.db.demo.extension.service.MongoService;

/**
 * @author lihuazeng
 */
public interface ArticleService extends MongoService<Article, String> {
    void createFooAndBar();
}
