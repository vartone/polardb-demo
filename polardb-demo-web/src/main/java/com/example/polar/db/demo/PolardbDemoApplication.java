package com.example.polar.db.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PolardbDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(PolardbDemoApplication.class, args);
    }

}
