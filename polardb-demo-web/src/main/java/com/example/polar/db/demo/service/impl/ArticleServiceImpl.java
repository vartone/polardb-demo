package com.example.polar.db.demo.service.impl;

import com.example.polar.db.demo.entity.Article;
import com.example.polar.db.demo.extension.service.MongoServiceImpl;
import com.example.polar.db.demo.repository.ArticleRepository;
import com.example.polar.db.demo.service.ArticleService;
import org.bson.Document;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author lihuazeng
 */
@Service
public class ArticleServiceImpl extends MongoServiceImpl<ArticleRepository, Article, String> implements ArticleService {
    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public void createFooAndBar() {
        this.mongoTemplate.insert(new Document("abc", 2), "foo");
        int i = 6/0;
        this.mongoTemplate.insert(new Document("xyz", 998), "bar");
    }
}
