package com.example.polar.db.demo;

import com.example.polar.db.demo.entity.Article;
import com.example.polar.db.demo.service.ArticleService;
import com.mongodb.ReadConcern;
import com.mongodb.ReadPreference;
import com.mongodb.TransactionOptions;
import com.mongodb.WriteConcern;
import com.mongodb.client.*;
import lombok.extern.slf4j.Slf4j;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
@SpringBootTest
class ArticleServiceTest {

    @Autowired
    private ArticleService articleService;
    @Autowired
    private MongoTemplate mongoTemplate;
    private ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());


    /**
     * 测试读写分离: 多线程insert；
     * 条件：connection String URI添加参数readPreference=secondaryPreferred
     * 监控页面-操作QPS数（个）
     * 主节点预期：10/60s = 0.17
     * 监控页面实际值0.17
     *
     * @throws InterruptedException
     */
    @Test
    public void test_write() throws InterruptedException {
        log.info("write start, dateTime："+ new Date());
        int sum = 10;
        final CountDownLatch writeCnt = new CountDownLatch(sum);
        for (int i = 0; i < sum; i++){
            executorService.submit(() -> {
                Article article = new Article();
                String random = UUID.randomUUID().toString();
                article.setTitle(random+"-title");
                article.setDescription(random+"-description");
                article.setContent( random+"-Content");
                articleService.create(article);
                writeCnt.countDown();
            });
        }
        writeCnt.await();
        log.info("结束时间ms："+ System.currentTimeMillis());
    }

    /**
     * 测试读写分离: 多线程query；
     * 条件：connection String URI添加参数readPreference=secondaryPreferred
     * 期望：在从节点查询
     * 控制台输出：落在从节点的ip
     *
     * 2020-09-25 15:06:03.770  INFO 7141 --- [pool-1-thread-6] org.mongodb.driver.connection            : Opened connection [connectionId{localValue:5, serverValue:63330}] to 47.x.x.221:3717
     * 2020-09-25 15:06:03.770  INFO 7141 --- [pool-1-thread-3] org.mongodb.driver.connection            : Opened connection [connectionId{localValue:9, serverValue:63331}] to 47.x.x.221:3717
     * 2020-09-25 15:06:03.775  INFO 7141 --- [pool-1-thread-8] org.mongodb.driver.connection            : Opened connection [connectionId{localValue:8, serverValue:63337}] to 47.x.x.221:3717
     * 2020-09-25 15:06:03.775  INFO 7141 --- [pool-1-thread-4] org.mongodb.driver.connection            : Opened connection [connectionId{localValue:10, serverValue:63334}] to 47.x.x.221:3717
     * 2020-09-25 15:06:03.775  INFO 7141 --- [pool-1-thread-2] org.mongodb.driver.connection            : Opened connection [connectionId{localValue:12, serverValue:63332}] to 47.x.x.221:3717
     * 2020-09-25 15:06:03.778  INFO 7141 --- [pool-1-thread-1] org.mongodb.driver.connection            : Opened connection [connectionId{localValue:6, serverValue:63336}] to 47.x.x.221:3717
     * 2020-09-25 15:06:03.780  INFO 7141 --- [pool-1-thread-5] org.mongodb.driver.connection            : Opened connection [connectionId{localValue:7, serverValue:63335}] to 47.x.x.221:3717
     * 2020-09-25 15:06:03.781  INFO 7141 --- [pool-1-thread-7] org.mongodb.driver.connection            : Opened connection [connectionId{localValue:11, serverValue:63333}] to 47.x.x.221:3717
     *
     * @throws InterruptedException
     */
    @Test
    public void test_read() throws InterruptedException {
        final List<String> ids = Arrays.asList("5f6d9468e6d2867373279c8a", "5f6d9468e6d2867373279c8b",
                "5f6d9468e6d2867373279c8c", "5f6d9468e6d2867373279c8f", "5f6d9468e6d2867373279c8d",
                "5f6d9468e6d2867373279c88", "5f6d9468e6d2867373279c89", "5f6d9468e6d2867373279c8e",
                "5f6d9469e6d2867373279c90", "5f6d9469e6d2867373279c91");
        log.info("read start, dateTime："+ new Date());
        final CountDownLatch readCnt = new CountDownLatch(ids.size());
        for (int i = 0; i < ids.size(); i++){
            final int idx = i;
            executorService.submit(() -> {
                Article article = articleService.detail(ids.get(idx));
                log.info("query result:{}", article);
                readCnt.countDown();
            });
        }
        readCnt.await();
    }

    /**
     * 测试强制读主: 多线程query；
     * 设置readPreference=primary
     * 期望：查询落在primary节点
     * 控制台输出：落在主节点的ip
     *
     * 2020-09-25 15:21:04.143  INFO 7299 --- [pool-1-thread-2] org.mongodb.driver.connection            : Opened connection [connectionId{localValue:10, serverValue:138745}] to 120.x.x.105:3717
     * 2020-09-25 15:21:04.143  INFO 7299 --- [pool-1-thread-7] org.mongodb.driver.connection            : Opened connection [connectionId{localValue:12, serverValue:138743}] to 120.x.x.105:3717
     * 2020-09-25 15:21:04.143  INFO 7299 --- [pool-1-thread-6] org.mongodb.driver.connection            : Opened connection [connectionId{localValue:8, serverValue:138744}] to 120.x.x.105:3717
     * 2020-09-25 15:21:04.143  INFO 7299 --- [pool-1-thread-1] org.mongodb.driver.connection            : Opened connection [connectionId{localValue:9, serverValue:138742}] to 120.x.x.105:3717
     * 2020-09-25 15:21:04.146  INFO 7299 --- [pool-1-thread-4] org.mongodb.driver.connection            : Opened connection [connectionId{localValue:7, serverValue:138749}] to 120.x.x.105:3717
     * 2020-09-25 15:21:04.146  INFO 7299 --- [pool-1-thread-8] org.mongodb.driver.connection            : Opened connection [connectionId{localValue:5, serverValue:138747}] to 120.x.x.105:3717
     * 2020-09-25 15:21:04.151  INFO 7299 --- [pool-1-thread-5] org.mongodb.driver.connection            : Opened connection [connectionId{localValue:11, serverValue:138748}] to 120.x.x.105:3717
     * 2020-09-25 15:21:04.151  INFO 7299 --- [pool-1-thread-3] org.mongodb.driver.connection            : Opened connection [connectionId{localValue:6, serverValue:138746}] to 120.x.x.105:3717
     */
    @Test
    public void test_readPrimary() throws InterruptedException {
        final List<String> ids = Arrays.asList("5f6d9468e6d2867373279c8a", "5f6d9468e6d2867373279c8b",
                "5f6d9468e6d2867373279c8c", "5f6d9468e6d2867373279c8f", "5f6d9468e6d2867373279c8d",
                "5f6d9468e6d2867373279c88", "5f6d9468e6d2867373279c89", "5f6d9468e6d2867373279c8e",
                "5f6d9469e6d2867373279c90", "5f6d9469e6d2867373279c91");
        log.info("read primary start, dateTime："+ new Date());
        mongoTemplate.setReadPreference(ReadPreference.primary());
        final CountDownLatch readCnt = new CountDownLatch(ids.size());
        for (int i = 0; i < ids.size(); i++){
            final int idx = i;
            executorService.submit(() -> {
                Article article = articleService.detail(ids.get(idx));
                log.info("query result:{}", article);
                readCnt.countDown();
            });
        }
        readCnt.await();
    }

    /**
     * mongoTemplate测试读偏好的作用范围；多线程query；
     * mongoTemplate单列不隔离,都落在primary节点
     * 期望：查询落在primary节点
     * 控制台输出：落在主节点的ip
     * 2020-09-25 15:21:04.143  INFO 7299 --- [pool-1-thread-2] org.mongodb.driver.connection            : Opened connection [connectionId{localValue:10, serverValue:138745}] to 120.x.x.105:3717
     * 2020-09-25 15:21:04.143  INFO 7299 --- [pool-1-thread-7] org.mongodb.driver.connection            : Opened connection [connectionId{localValue:12, serverValue:138743}] to 120.x.x.105:3717
     */
    @Test
    public void test_readPreferenceScope() throws InterruptedException {
        List<String> ids = Arrays.asList("5f6d9468e6d2867373279c8f", "5f6d9468e6d2867373279c8e");
        log.info("read primary start, dateTime："+ new Date());
        mongoTemplate.setReadPreference(ReadPreference.primary());
        Article article = mongoTemplate.findById("5f6d9469e6d2867373279c91", Article.class);
        final CountDownLatch readCnt = new CountDownLatch(ids.size());
        for (int i = 0; i < ids.size(); i++){
            final int idx = i;
            executorService.submit(() -> {
                Article art = articleService.detail(ids.get(idx));
                log.info("query result:{}", art);
                readCnt.countDown();
            });
        }
        readCnt.await();
    }

    /**
     * db测试读偏好的作用范围；多线程query；
     * 期望：article1查询落在primary节点, article2查询落在secondary节点
     *
     * 控制台输出：落在主节点的ip
     * 2020-09-25 15:50:11.936  INFO 7587 --- [           main] org.mongodb.driver.connection            : Opened connection [connectionId{localValue:5, serverValue:140182}] to 120.55.219.105:3717
     * 2020-09-25 15:50:12.264  INFO 7587 --- [           main] org.mongodb.driver.connection            : Opened connection [connectionId{localValue:6, serverValue:64319}] to 47.114.234.221:3717
     */
    @Test
    public void test_readPreferenceScope2() {
        log.info("read primary start, dateTime："+ new Date());
        Document article1 = mongoTemplate.getDb().withReadPreference(ReadPreference.primary()).getCollection("article").find(new Document("_id", new ObjectId("5f6d9468e6d2867373279c8e"))).first();
        Article article2 = mongoTemplate.findById("5f6d9469e6d2867373279c91", Article.class);
        log.info("query result:{}", article1);
        log.info("query result:{}", article2);
    }

    /**
     * 测试事务，原生API；
     * 需要提交创建好集合
     */
    @Test
    public void test_transaction1() {
        String uri = "mongodb://root";
        final MongoClient client = MongoClients.create(uri);

        // Prereq: Create collections. CRUD operations in transactions must be on existing collections.


        // Step 1: Start a client session.
        final ClientSession clientSession = client.startSession();

        // Step 2: Optional. Define options to use for the transaction.
        TransactionOptions txnOptions = TransactionOptions.builder()
                .readPreference(ReadPreference.primary())
                .readConcern(ReadConcern.LOCAL)
                .writeConcern(WriteConcern.MAJORITY)
                .build();

        // Step 3: Define the sequence of operations to perform inside the transactions.
        TransactionBody<String> txnBody = () -> {
            MongoCollection<Document> coll1 = client.getDatabase("test").getCollection("foo");
            MongoCollection<Document> coll2 = client.getDatabase("test").getCollection("bar");

            // Important:: You must pass the session to the operations.
            coll1.insertOne(clientSession, new Document("abc", 1));
            int i = 5/0; // 模拟异常，会进行回滚
            coll2.insertOne(clientSession, new Document("xyz", 999));
            return "Inserted into collections in different databases";
        };

        try {
            // Step 4: Use .withTransaction() to start a transaction, execute the callback, and commit (or abort on error).
            clientSession.withTransaction(txnBody, txnOptions);
        } catch (RuntimeException e) {
            log.info("exception:", e);
            // some error handling
        } finally {
            clientSession.close();
        }
    }

    /**
     * 测试事务，原生API,spring boot；
     *
     * 定义MongoTransactionManager的bean即可开启spring boot mongo的事务
     */
    @Test
    public void test_transaction2() {
        articleService.createFooAndBar();
    }
}
