package com.example.polar.db.demo;

import com.example.polar.db.demo.service.impl.RedisService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;

@Slf4j
@SpringBootTest
class RedisTest {

    @Autowired
    private RedisService<String> cachingService;


    /**
     * 连接集群方式-代理模式
     * 使用jedis连接，插入8个随机k-v;控制台监控页面查看各节点情况
     * 性能监控：数据节点-keys；Proxy-Max RT
     *
     * 8个key基本被路由到各个分片
     */
    @Test
    void test_proxyConnection() {
        log.info("当前时间："+ new Date());
        for (int i = 0; i < 8; i++) {
            cachingService.setCacheObject(UUID.randomUUID().toString(), "1");
        }
    }

    /**
     * 连接集群方式-直连模式：需要在ECS上操作，因为需要在同一网段
     * 使用jedis连接，插入8个随机k-v;控制台监控页面查看各节点情况
     * 发布到ECS上
     * curl http://127.0.0.1:8080/users/caching/test
     *
     * 跟代理模式一样
     */
    @Test
    void test_DirectConnection() {
    }

    /**
     * 测试直连和代理是否兼容
     * 直连模式插入数据，代理模式读取数据；代理模式插入数据，直连模式读取数据（ECS）
     *
     * 代理模式能读取直连模式写入的k-v
     * 再ECS上用直连模式也能读到代理模式写入的k-v（curl http://127.0.0.1:8080/users/caching/66ff3bd2-ca83-4fd9-8bc3-b76dac6ae023）
     */
    @Test
    void test_DirectAndProxyCompatibility() {
        // 66ff3bd2-ca83-4fd9-8bc3-b76dac6ae023，这是代理模式写入的key
        String key = "9cf47416-8e80-4bec-a4de-673e18c5c004";// 这个是直连模式写入的key=9cf47416-8e80-4bec-a4de-673e18c5c004
        Object v = cachingService.getCacheObject(key);
        log.info("get Direct Mode k-v, key:{}, value:{}", key, v);
    }

    /**
     * jedis连接测试断开重连
     * 释放公网地址，再重新申请
     *
     */
    @Test
    void test_Reconnection() {
    }

}
