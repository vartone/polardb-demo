package com.example.polar.db.demo;

import com.example.polar.db.demo.entity.User;
import com.example.polar.db.demo.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
@SpringBootTest
class PolardbDemoApplicationTests {

    @Autowired
    private UserService userService;
    private ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());


    /**
     * 集群地址，模式为可读可写
     * 测试读写分离,执行10次写操作
     * 性能监控单位时间秒s；扫描行数-每秒插入的行数
     *
     *
     */
    @Test
    void test_Write() throws InterruptedException {
        log.info("当前时间ms："+ System.currentTimeMillis());
        int sum = 10;
        final CountDownLatch writeCnt = new CountDownLatch(sum);
        for (int i = 0; i < sum; i++){
            executorService.submit(() -> {
                User user = new User();
                String random = UUID.randomUUID().toString();
                user.setName(random+"-Name");
                user.setAddress(random+"-Address");
                user.setEmail( random+"@email.com");
                userService.insert(user);
                writeCnt.countDown();
            });
        }
        writeCnt.await();
        log.info("结束时间ms："+ System.currentTimeMillis());
    }

    /**
     * 集群地址，模式为可读可写
     * 测试读写分离,分别执行20次读操作
     * 性能监控单位时间秒s；操作-每秒SELECT数
     * 主：15:21=7.12;15:21=6.82;    7.12-6.82=0.3
     * 读：15:21=7.15;15:21=6.77;    7.15-6.77=0.38
     * 20/60s = 0.333
     * 主库接受读
     *
     */
    @Test
    void test_Read() throws InterruptedException {
        long start = System.currentTimeMillis();
        log.info("当前时间ms："+ new Date(start));
        log.info("当前时间ms："+ start);
        int sum = 20;
        final CountDownLatch readCnt = new CountDownLatch(sum);
        Set<String> userIds = new HashSet<>();
        for (int i = 0; i < sum; i++){
            executorService.submit(() -> {
                Random random =  new Random();
                User record = userService.queryById(random.nextInt(20) + 1);
                if (record != null) {
                    userIds.add(record.getName());
                }
                log.info("get user:"+ record);
                readCnt.countDown();
            });
        }
        readCnt.await();
        long end = System.currentTimeMillis();
        log.info("耗时ms："+ (end - start));
        log.info("user size："+ userIds.size());
    }

    /**
     * 集群地址，模式为可读可写
     * 测试Hint语法
     * 强制查从库
     * 开始时间：17:11:11
     * 结果：（性能监控-扫描行数）
     *     master：每秒读取的行数：0.02
     *     slave: 每秒读取的行数：0.33 （20/60s）
     * 能强制路由到读节点查询
     */
    @Test
    void test_Hint() throws InterruptedException {
        long start = System.currentTimeMillis();
        log.info("当前时间ms："+ new Date(start));
        log.info("当前时间ms："+ start);
        int sum = 20;
        final CountDownLatch readCnt = new CountDownLatch(sum);
        Set<String> userIds = new HashSet<>();
        for (int i = 0; i < sum; i++){
            executorService.submit(() -> {
                Random random =  new Random();
                User record = userService.queryByIdFromSlave(random.nextInt(20) + 1);
                if (record != null) {
                    userIds.add(record.getName());
                }
                log.info("get user:"+ record);
                readCnt.countDown();
            });
        }
        readCnt.await();
        long end = System.currentTimeMillis();
        log.info("耗时ms："+ (end - start));
        log.info("user size："+ userIds.size());
    }

    /**
     * 集群地址，模式为可读可写
     * 测试读写分离一致问题
     * 先写入数据，再强制查从库
     * 开始时间：16:58:02
     * 结果：（性能监控-扫描行数）
     *     master：每秒插入的行数：0.08;每秒读取的行数：0.04
     *     slave: 每秒插入的行数：0;每秒读取的行数：0.08
     * 能读取到插入的数据
     */
    @Test
    void test_ConsistLevel() throws InterruptedException {
        log.info("当前时间："+ new Date());
        for (int i = 0; i < 5; i++){
            User user = new User();
            String random = UUID.randomUUID().toString();
            user.setName(random+"-Name");
            user.setAddress(random+"-Address");
            user.setEmail( random+"@email.com");
            userService.insert(user);
        }

        List<User> user2 = userService.queryByIdsFromSlave("26,27,28,29,30");
        log.info("user："+ user2);
    }

    /**
     * 集群地址，模式为可读可写
     * 高可用
     * 重启主节点，应用能否正常访问数据库。启动本地web项目
     *
     */
    @Test
    void test_Availability() {
        // 启动web应用；进行写请求
        // POST http://127.0.0.1:8080/users
        // 可以正常插入数据
    }


    /**
     * 集群地址，模式为可读可写
     * 模拟闪断重连
     * 公网地址释放 and 重新申请，应用能否正常访问数据库。启动本地web项目
     *
     */
    @Test
    void test_Connection() {
        // 启动web应用；进行写请求
        // GET http://127.0.0.1:8080/users/2
        // 重新申请后能正常访问
    }



}
