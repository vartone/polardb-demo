package com.example.polar.db.demo.common;

import lombok.Getter;
import lombok.Setter;

/**
 * 分页查询基础参数
 *
 * @author lihuazeng
 */
@Getter
@Setter
public class PageQuery {
    private Integer page = 1;
    private Integer size = 10;
}
