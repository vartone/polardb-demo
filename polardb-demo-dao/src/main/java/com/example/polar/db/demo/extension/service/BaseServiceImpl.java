package com.example.polar.db.demo.extension.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.polar.db.demo.common.Filter;
import com.example.polar.db.demo.common.PageQueryRequest;
import com.example.polar.db.demo.common.PageQueryResponse;

/**
 * @author lihuazeng
 */
public class BaseServiceImpl <M extends BaseMapper<T>, T> extends ServiceImpl<M, T> implements BaseService<T> {
    @Override
    public PageQueryResponse<T> pageQuery(PageQueryRequest request) {
        boolean isAsc = false;
        if (StringUtils.isNotBlank(request.getSortOrder())){
            isAsc = "ASC".equalsIgnoreCase(request.getSortOrder());
        }
        String orderBy = "created_at";
        if (StringUtils.isNotBlank(request.getSortColumn())) {
            orderBy = request.getSortColumn();
        }
        QueryWrapper<T> queryWrapper =  new QueryWrapper<>();
        queryWrapper.eq("deleted", false);
        if (isAsc) {
            queryWrapper.orderByAsc(orderBy.split(","));
        } else {
            queryWrapper.orderByDesc(orderBy.split(","));
        }
        if (request.getFilters() != null && !request.getFilters().isEmpty()) {
                request.getFilters().forEach(f -> {
                switch (f.getCondition()){
                    case Filter.CONDITION_EQ :
                        queryWrapper.eq(f.getKey(), f.getValue());
                        break;
                    case Filter.CONDITION_BETWEEN :
                        if (f.getFrom() != null){
                            queryWrapper.gt(f.getKey(), f.getFrom());
                        }
                        if (f.getTo() != null){
                            queryWrapper.le(f.getKey(), f.getTo());
                        }
                        break;
                    case Filter.CONDITION_IN :
                        if (f.getValue() != null){
                            String v = (String)f.getValue();
                            queryWrapper.in(f.getKey(), v.split(","));
                        }
                        break;
                    case Filter.CONDITION_LIKE :
                        queryWrapper.like(f.getKey(), f.getValue());
                        break;
                    case Filter.CONDITION_NIN :
                        if (f.getValue() != null){
                            String v = (String)f.getValue();
                            queryWrapper.notIn(f.getKey(), v.split(","));
                        }
                        break;
                    default:
                        break;
                }
            });
        }
        if (StringUtils.isNotBlank(request.getGroupBy())){
            queryWrapper.groupBy(request.getGroupBy().split(","));
        }
        Page<T> page = new Page<>(request.getPage(), request.getSize());
        page = this.page(page, queryWrapper);
        PageQueryResponse<T> pageQueryResponse = new PageQueryResponse<>();
        pageQueryResponse.setPages(Math.toIntExact(page.getPages()));
        pageQueryResponse.setTotal(page.getTotal());
        pageQueryResponse.setSize(Math.toIntExact(page.getSize()));
        pageQueryResponse.setList(page.getRecords());
        return pageQueryResponse;
    }
}
