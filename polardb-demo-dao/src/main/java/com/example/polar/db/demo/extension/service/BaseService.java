package com.example.polar.db.demo.extension.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.polar.db.demo.common.PageQueryRequest;
import com.example.polar.db.demo.common.PageQueryResponse;

/**
 * @author lihuazeng
 */
public interface BaseService<T>  extends IService<T> {
    /**
     * 分页查询
     * @param request
     * @return
     */
    PageQueryResponse<T> pageQuery(PageQueryRequest request);
}
