package com.example.polar.db.demo.common;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 分页查询返回数据
 *
 * @author lihuazeng
 */
@Getter
@Setter
public class PageQueryResponse<T> {
    private Long total;
    private Integer pages;
    private Integer size;
    private List<T> list;
}
