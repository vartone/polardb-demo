package com.example.polar.db.demo.common;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * 过滤条件
 *
 * @author lihuazeng
 */
@Setter
@Getter
@Accessors(chain = true)
public class Filter {
    public static final String CONDITION_EQ = "=";
    public static final String CONDITION_LIKE = "like";
    public static final String CONDITION_IN = "in";
    public static final String CONDITION_NIN = "nin";
    public static final String CONDITION_BETWEEN = "between";

    private String key;
    private String value;
    private String condition;
    private String from;
    private String to;
}
