package com.example.polar.db.demo.entity;


import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


/**
 * @author lihuazeng
 */
@Getter
@Setter
@TableName("t_user")
@ToString
public class User extends BaseEntity {
    private String name;
    private String address;
    private String email;
}
