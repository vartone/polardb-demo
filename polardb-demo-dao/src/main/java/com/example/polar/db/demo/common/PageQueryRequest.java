package com.example.polar.db.demo.common;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 分页查询请求
 *
 * @author lihuazeng
 */
@Getter
@Setter
public class PageQueryRequest extends PageQuery {
    private String sortColumn;
    private String sortOrder;
    private String groupBy;
    private List<Filter> filters;
}
