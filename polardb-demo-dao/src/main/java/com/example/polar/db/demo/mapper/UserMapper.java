package com.example.polar.db.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.polar.db.demo.entity.User;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author lihuazeng
 */
public interface UserMapper extends BaseMapper<User> {
    @Select("/*FORCE_SLAVE*/ SELECT * FROM t_user WHERE id = #{id}")
    User queryByIdFromSlave(Integer id);

    @Select("/*FORCE_SLAVE*/ SELECT * FROM t_user WHERE id in (${id})")
    List<User> queryByIdsFromSlave(String id);
}
