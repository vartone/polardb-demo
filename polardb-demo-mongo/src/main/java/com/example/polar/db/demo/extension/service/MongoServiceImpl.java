package com.example.polar.db.demo.extension.service;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUtil;
import com.example.polar.db.demo.common.Filter;
import com.example.polar.db.demo.common.PageQueryRequest;
import com.example.polar.db.demo.common.PageQueryResponse;
import com.example.polar.db.demo.entity.BaseMongoEntity;
import com.mongodb.ReadPreference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.regex.Pattern;

/**
 * 单笔操作service层基本实现
 *
 * @author lihuazeng
 */
public class MongoServiceImpl<R extends MongoRepository<T, ID>, T, ID> implements MongoService<T, ID> {
    @Autowired(required = false)
    protected R repository;
    @Autowired
    protected MongoTemplate mongoTemplate;

    @Override
    public T create(T t) {
        return repository.insert(t);
    }

    @Override
    public Boolean delete(ID id) {
        Optional<T> t = repository.findById(id);
        t.ifPresent(po -> {
            if (po instanceof BaseMongoEntity) {
                BaseMongoEntity b = (BaseMongoEntity) po;
                b.setDeleted(true);
                repository.save(po);
            }
        });
        return true;
    }

    @Override
    public T update(T t) {
        mongoTemplate.setReadPreference(ReadPreference.primary());
        return repository.save(t);
    }

    @Override
    public T detail(ID id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public List<T> listByIds(List<ID> ids) {
        return CollectionUtil.list(false, repository.findAllById(ids));
    }

    @Override
    public PageQueryResponse<T> pageQuery(PageQueryRequest request) {
        Sort sort = buildSort(request);
        Query query = buildQuery(request);
        Class<T> type = this.getEntityClass();
        long total = mongoTemplate.count(query, type);
        PageRequest pageRequest = PageRequest.of(request.getPage() - 1, request.getSize(), sort);
        List<T> productOptions = mongoTemplate.find(query.with(pageRequest), type);
        Page<T> page = new PageImpl<>(productOptions, pageRequest, total);
        PageQueryResponse<T> pageQueryResponse = new PageQueryResponse<>();
        pageQueryResponse.setPages(page.getTotalPages());
        pageQueryResponse.setTotal(page.getTotalElements());
        pageQueryResponse.setSize(page.getSize());
        pageQueryResponse.setList(productOptions);
        return pageQueryResponse;
    }

    private Sort buildSort(PageQueryRequest request) {
        Sort sort;
        Sort.Direction direction;
        if (!StringUtils.isEmpty(request.getSortOrder())) {
            direction = "ASC".equalsIgnoreCase(request.getSortOrder()) ? Sort.Direction.ASC : Sort.Direction.DESC;
        } else {
            direction = Sort.Direction.DESC;
        }
        if (!StringUtils.isEmpty(request.getSortColumn())) {
            sort = Sort.by(direction, request.getSortColumn().split(","));
        } else {
            sort = Sort.by(direction, "updatedAt");
        }
        return sort;
    }

    private Query buildQuery(PageQueryRequest request) {
        Query query = new Query();
        query.addCriteria(Criteria.where("deleted").is(false));
        if (request.getFilters() != null && request.getFilters().size() > 0) {
            request.getFilters().forEach(e -> {
                switch (e.getCondition()) {
                    case Filter.CONDITION_EQ:
                        query.addCriteria(Criteria.where(e.getKey()).is(e.getValue()));
                        break;
                    case Filter.CONDITION_BETWEEN:
                        Criteria c = new Criteria();
                        query.addCriteria(c.andOperator(Criteria.where(e.getKey()).lt(DateUtil.parse(e.getTo()).toJdkDate()),
                                Criteria.where(e.getKey()).gte(DateUtil.parse(e.getFrom()).toJdkDate())));
                        break;
                    case Filter.CONDITION_IN:
                        String v = e.getValue();
                        query.addCriteria(Criteria.where(e.getKey()).in(Arrays.asList(v.split(","))));
                        break;
                    case Filter.CONDITION_LIKE:
                        Pattern pattern = Pattern.compile("^.*" + e.getValue() + ".*$", Pattern.CASE_INSENSITIVE);
                        query.addCriteria(Criteria.where(e.getKey()).regex(pattern));
                        break;
                    case Filter.CONDITION_NIN:
                        v = e.getValue();
                        query.addCriteria(Criteria.where(e.getKey()).nin(Arrays.asList(v.split(","))));
                        break;
                    default:
                        break;
                }
            });
        }
        return query;
    }

    @Override
    public List<T> createBatch(List<T> ts) {
        return repository.insert(ts);
    }

    @Override
    public List<T> saveOrUpdateBatch(List<T> ts) {
        return repository.saveAll(ts);
    }

    @Override
    public List<T> listByPage(PageQueryRequest request) {
        Sort sort = buildSort(request);
        Query query = buildQuery(request);
        Class<T> type = this.getEntityClass();
        PageRequest pageRequest = PageRequest.of(request.getPage() - 1, request.getSize(), sort);
        return mongoTemplate.find(query.with(pageRequest), type);
    }

    @Override
    public long queryCount(PageQueryRequest request) {
        Query query = buildQuery(request);
        Class<T> type = this.getEntityClass();
        return mongoTemplate.count(query, type);
    }
}
