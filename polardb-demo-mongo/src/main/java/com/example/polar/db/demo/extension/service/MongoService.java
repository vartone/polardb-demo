package com.example.polar.db.demo.extension.service;

import com.example.polar.db.demo.common.PageQueryRequest;
import com.example.polar.db.demo.common.PageQueryResponse;

import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * 单笔操作service层接口
 *
 * @author lihuazeng
 */
public interface MongoService<T, ID> {
    /***/
    default Class<T> getEntityClass() {
        return (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[1];
    }

    T create(T t);

    T update(T t);

    Boolean delete(ID id);

    T detail(ID id);

    List<T> listByIds(List<ID> ids);

    PageQueryResponse<T> pageQuery(PageQueryRequest request);

    List<T> createBatch(List<T> ts);

    List<T> saveOrUpdateBatch(List<T> ts);

    List<T> listByPage(PageQueryRequest request);

    long queryCount(PageQueryRequest request);
}
