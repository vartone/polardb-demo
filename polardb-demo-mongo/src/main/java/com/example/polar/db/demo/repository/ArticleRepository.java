package com.example.polar.db.demo.repository;

import com.example.polar.db.demo.entity.Article;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author lihuazeng
 */
public interface ArticleRepository extends MongoRepository<Article, String> {
}
