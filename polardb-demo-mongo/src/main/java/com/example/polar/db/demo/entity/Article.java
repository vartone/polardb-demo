package com.example.polar.db.demo.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


/**
 * @author lihuazeng
 */
@Getter
@Setter
@ToString
public class Article extends BaseMongoEntity {
    private String title;
    private String description;
    private String content;
}
